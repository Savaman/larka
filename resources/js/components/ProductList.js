import React from "react";


class ProductList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
             products:[]
        }
    }



    componentDidUpdate(prevProps, prevState, snapshot) {
        console.dir("Prev props:" + prevProps.categoryID);
        console.dir("Current props:" + this.props.categoryID);


        if(prevProps.categoryID != this.props.categoryID){
            // AJAX
            fetch("/api/products/" + this.props.categoryID)
                .then(res => res.json())
                .then(
                    (result) => {
                        this.setState({
                            //    isLoaded: true,
                            products: result
                        });
                    },
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    }
                )

        }

    }

    componentDidMount() {

        console.log(this.props.categoryID);


        fetch("/api/products/")
            .then(res => res.json())
            .then(
                (result) => {

                    console.log(result);

                    this.setState({
                        //    isLoaded: true,
                        products: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }


    render(){

        console.log( "Category ID:  " + this.props.categoryID);

        return (
            <div>


                <ul>

                    {this.state.products.map(item => (
                        <li key={item.name} onClick={ () => {this.props.updateCategory(item.id)}}>
                            {item.name}
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}
export default ProductList;
