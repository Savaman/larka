import React from 'react';
import ReactDOM from 'react-dom';


function Footer() {
    return (
        <div className="footer">
            <div className="footer-wrapper">
            </div>
        </div>
    );
}

export default Footer;

// DOM element
if (document.getElementById('footer')) {
    ReactDOM.render(<Footer />, document.getElementById('footer'));
}
