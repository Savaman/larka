import React from 'react';
import ReactDOM from 'react-dom';
import {Range} from "rc-slider";


let onSliderChange = value => {
    console.log(value);
    this.setState({
        value,
    });
};

class Sidebar extends React.Component {


    constructor(props) {
        super(props);
        this.setUpperValue = this.setUpperValue.bind(this);
        this.setLowerValue = this.setLowerValue.bind(this);
        this.state = {
            lowerBound: 10,
            upperBound: 90,
            value: [20, 40],
            products:[]
        };
    }

    componentDidMount() {
        fetch("/api/categories")
            .then(res => res.json())
            .then(
                (result) => {

                    console.log(result);

                     this.setState({
                     //    isLoaded: true,
                         products: result
                     });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    setUpperValue = event => {

        let value = [this.state.value[0] ,+event.target.value];
    //   console.log(value);

       this.setState({
            value,
        });


        console.log(this.state);
    }

    setLowerValue = event => {
        let value = [+event.target.value, this.state.value[1]];


        this.setState({
            value,
        });


        console.log(this.state);
    }
    onSliderChange = value => {

        this.setState({
            value,
        });
    };
    render() {
        return (
            <div className="sidebar">
                <div className="sidebar-wrapper">
                    <div id="range" className="price-wrapper">
                        <form className="range-form">
                            <input type="number"
                                   value={this.state.value[0]}
                                   onChange={this.setLowerValue}
                                   className="from-price price-input form-control"/>
                            <span> - </span>
                            <input type="number"
                                   value={this.state.value[1]}
                                   onChange={this.setUpperValue}
                                   className="to-price price-input form-control"/>
                            <button className="range-button btn btn-primary ">OK</button>
                            <Range min={this.state.lowerBound}
                                   max={this.state.upperBound}
                                   defaultValue={this.state.value}
                                   value={this.state.value}
                                   onChange={this.onSliderChange}/>
                        </form>


                        <ul>
                            {/*{products.map(item => (*/}
                            {/*    <li key={item.name}>*/}
                            {/*        {item.name}*/}
                            {/*    </li>*/}
                            {/*))}*/}
                        </ul>

                    </div>
                </div>
            </div>
        );
    }
}

export default Sidebar;

// DOM element
if (document.getElementById('sidebar')) {
    ReactDOM.render(<Sidebar />, document.getElementById('sidebar'));
}
