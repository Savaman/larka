import React from "react";
import ReactDOM from "react-dom";
import CategoryList from "./CategoryList";
import ProductList from "./ProductList";
import {BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom';
import Header from "./header";
import Main from "./main";
import Footer from "./footer";
import Contact from "./Contact";


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categoryID: 0,
            defaultValue: [0, 100],
        }
    }

    updateCategory = (id) => {
        this.setState({
            categoryID: id
        });
    }

    render() {
        return (
            <Router>
                <Header/>

                <Switch>
                    <Route path="/contact">
                        <Contact/>
                    </Route>
                    <Route path="/about">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Nisl tincidunt eget nullam non. Quis hendrerit dolor magna eget
                        est lorem ipsum dolor sit. Volutpat odio facilisis mauris sit amet massa. Commodo odio aenean
                        sed adipiscing diam donec adipiscing tristique. Mi eget mauris pharetra et. Non tellus orci ac
                        auctor augue. Elit at imperdiet dui accumsan sit. Ornare arcu dui vivamus arcu felis. Egestas
                        integer eget aliquet nibh praesent. In hac habitasse platea dictumst quisque sagittis purus.
                        Pulvinar elementum integer enim neque volutpat ac.
                        Senectus et netus et malesuada. Nunc pulvinar sapien et ligula ullamcorper malesuada proin.
                        Neque convallis a cras semper auctor. Libero id faucibus nisl tincidunt eget. Leo a diam
                        sollicitudin tempor id. A lacus vestibulum sed arcu non odio euismod lacinia. In tellus integer
                        feugiat scelerisque. Feugiat in fermentum posuere urna nec tincidunt praesent. Porttitor rhoncus
                        dolor purus non enim praesent elementum facilisis. Nisi scelerisque eu ultrices vitae auctor eu
                        augue ut lectus. Ipsum faucibus vitae aliquet nec ullamcorper sit amet risus. Et malesuada fames
                        ac turpis egestas sed. Sit amet nisl suscipit adipiscing bibendum est ultricies. Arcu ac tortor
                        dignissim convallis aenean et tortor at. Pretium viverra suspendisse potenti nullam ac tortor
                        vitae purus. Eros donec ac odio tempor orci dapibus ultrices. Elementum nibh tellus molestie
                        nunc. Et magnis dis parturient montes nascetur. Est placerat in egestas erat imperdiet.
                        Consequat interdum varius sit amet mattis vulputate enim.
                        Sit amet nulla facilisi morbi tempus. Nulla facilisi cras fermentum odio eu. Etiam erat velit
                        scelerisque in dictum non consectetur a erat. Enim nulla aliquet porttitor lacus luctus accumsan
                        tortor posuere. Ut sem nulla pharetra diam. Fames ac turpis egestas maecenas. Bibendum neque
                        egestas congue quisque egestas diam. Laoreet id donec ultrices tincidunt arcu non sodales neque.
                        Eget felis eget nunc lobortis mattis aliquam faucibus purus. Faucibus interdum posuere lorem
                                ipsum dolor sit.</p>
                    </Route>
                    <Route path="/services">
                        <CategoryList updateCategory={this.updateCategory}/>
                        <ProductList categoryID={this.state.categoryID}/>
                    </Route>
                    <Route>
                        <Main/>
                    </Route>
                </Switch>

                <Footer/>
            </Router>
        )
    }
}

export default App;

if (document.getElementById('App')) {
    ReactDOM.render(<App/>, document.getElementById('App'));
}

